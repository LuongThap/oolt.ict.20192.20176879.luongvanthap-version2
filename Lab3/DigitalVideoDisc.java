/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3;

public class DigitalVideoDisc {

    private String title;
    private String category;
    private String director;
    private int length;
    private float cost;

    public DigitalVideoDisc() {
        
    }

    public DigitalVideoDisc(String title) {
        super();
        this.title = title;
    }
   	
	public DigitalVideoDisc(String category, String title) {
		this.category = category;
		this.title = title;
	}
	public DigitalVideoDisc(String director, String category, String title) {
		this.director = director;
		this.category = category;
		this.title = title;
	}
	
	public DigitalVideoDisc(String director, String category, String title, int length, float cost) {
		this.director = director;
		this.category = category;
		this.title = title;
		this.length = length;
		this.cost = cost;
	}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int lenght) {
        this.length = lenght;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }
    
    

}
