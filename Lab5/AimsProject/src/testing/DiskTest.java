/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order.Order;

/**
 *
 * @author tenhandsome
 */
public class DiskTest {

    public static void main(String[] args) {
       Order order = new Order();
       order.addDigitalVideoDisc(new DigitalVideoDisc("Title 1"));
       order.addDigitalVideoDisc(new DigitalVideoDisc("Title 2"));
       order.addDigitalVideoDisc(new DigitalVideoDisc("Title 3"));
       order.addDigitalVideoDisc(new DigitalVideoDisc("Title 4"));
       
       System.out.println("The lucky disc is : "+ order.getALuckyItem().getTitle());
       
       DigitalVideoDisc disc = new DigitalVideoDisc("Barry Allen");
       System.out.println("Result when search disc is with Allen : " + disc.search("Allen") );
              
    }
}
