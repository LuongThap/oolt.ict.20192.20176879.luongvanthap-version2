
package hust.soict.ictglobal.aims.Aims;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order.Order;

public class Aims {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
		
		Order anOrder = new Order();
		
		// Create a new dvd object and set the field
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		// Add the dvd to the order
		anOrder.addDigitalVideoDisc(dvd1);
		//System.out.println(anOrder.qtyOrdered());
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("John Musker", "Animation", "Alladin", 90, 18.99f);
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Star Wars");
		dvd3.setCategory("Science Fiction");
		dvd3.setCost(24.95f);
		dvd3.setDirector("George Lucas");
		dvd3.setLength(124);
		anOrder.addDigitalVideoDisc(dvd3);
		
		//anOrder.removeDigitalVideoDisc(dvd3);
		
		System.out.print("Total cost is: ");
		System.out.println(anOrder.totalCost());
    }
}
