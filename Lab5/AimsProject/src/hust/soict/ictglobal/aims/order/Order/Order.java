package hust.soict.ictglobal.aims.order.Order;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import java.lang.reflect.Array;
import java.util.Random;

public class Order {

    int qtyOrdered = 0;
    public static final int MAX_NUMBERS_ORDERED = 10;
    private DigitalVideoDisc[] itemsOrdered = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyOrdered <= MAX_NUMBERS_ORDERED) {
            itemsOrdered[qtyOrdered] = disc;
            qtyOrdered++;
        }
        System.out.println("The disc has been added");
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        for (int i = 0; i < qtyOrdered; i++) {
            if (itemsOrdered[i] == disc) {
                for (int j = i; j < qtyOrdered; j++) {
                    itemsOrdered[j] = itemsOrdered[j + 1];
                }
                System.out.println("Removed item " + disc.getTitle() + " from the list.");
                this.qtyOrdered--;
                break;
            }
        }
    }

    public float totalCost() {
        float total = 0;
        for (int i = 0; i < qtyOrdered; i++) {
            total += itemsOrdered[i].getCost();
        }
        return total;

    }
    
    public DigitalVideoDisc getALuckyItem(){
        int min = 0;
        int max = this.qtyOrdered;
        int rand = new Random().nextInt((max - min) + 1) + min;
        return itemsOrdered[rand];
    }
}
