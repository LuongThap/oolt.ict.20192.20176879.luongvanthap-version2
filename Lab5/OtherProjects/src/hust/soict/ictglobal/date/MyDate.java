/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.ictglobal.date;

import java.util.Scanner;

/**
 *
 * @author tenhandsome
 */
public class MyDate {

    private String day;
    private String month;
    private int year;

    public MyDate() {
    }

    public MyDate(String day, String month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String date) {
        accept(date);
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void accept(String date) {
        try {
            String[] data = date.split(" ");
            this.month = data[0];
            this.day = data[1];
            this.year = Integer.parseInt(data[2]);
        } catch (Exception e) {
            System.out.println("Invalid data input");
        }

    }

    public void print() {
        System.out.println("The current date is: " + this.month + " " + this.day + " " + this.year);
    }

    public static void main(String[] args) {
        var date = new MyDate();
        System.out.print("Please enter the date : ");
        String dateStr = new Scanner(System.in).nextLine();
        date.accept(dateStr);
        date.print();
    }
}
