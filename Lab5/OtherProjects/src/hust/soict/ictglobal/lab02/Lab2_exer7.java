package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Lab2_exer7 {

    public static void main(String[] args) {
        System.out.print("Enter width of matrix : ");
        int width = Integer.parseInt(new Scanner(System.in).nextLine());

        System.out.print("Enter height of matrix : ");
        int height = Integer.parseInt(new Scanner(System.in).nextLine());

        int firstMaxtrix[][] = new int[width][height];
        int secondMaxtrix[][] = new int[width][height];
        try {
            //        add data first matrix
            System.out.println("First matrix: ");
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    System.out.print("Enter data of element [" + (i + 1) + "][" + (j + 1) + "] : ");
                    firstMaxtrix[i][j] = Integer.parseInt(new Scanner(System.in).nextLine());
                }
            }
            //        add data second matrix
            System.out.println("Second matrix: ");
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    System.out.print("Enter data of element [" + (i + 1) + "][" + (j + 1) + "] : ");
                    secondMaxtrix[i][j] = Integer.parseInt(new Scanner(System.in).nextLine());
                }
            }
            
            // adding
            int result[][] = new int[width][height];
            System.out.println("Second matrix: ");
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    result[i][j] = firstMaxtrix[i][j] + secondMaxtrix[i][j];
                }
            }
            // print result
            System.out.println("Result : ");
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    System.out.print(result[i][j] + "   ");
                }
                System.out.println();
            }
        } catch (Exception e) {
            System.out.println("Invalid data");
        }

    }
}
