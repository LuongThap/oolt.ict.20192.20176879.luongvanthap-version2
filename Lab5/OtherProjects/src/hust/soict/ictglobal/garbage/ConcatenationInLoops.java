/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.ictglobal.garbage;

/**
 *
 * @author tenhandsome
 */
public class ConcatenationInLoops {

    public void CompareExecutionTime() {

        long start = System.currentTimeMillis();
        String str = "";
        for (int i = 0; i < 19999; i++) {
            str += "1";
        }
        long end = System.currentTimeMillis();
        System.out.println("StringBuffer takes " + (end - start) + " miniseconds");
        start = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 19999; i++) {
            sb.append("a");
        }
        end = System.currentTimeMillis();
        System.out.println("StringBuilder takes " + (end - start) + " miniseconds");
    }
    
    public static void main(String[] args) {
        new ConcatenationInLoops().CompareExecutionTime();
    }
}
