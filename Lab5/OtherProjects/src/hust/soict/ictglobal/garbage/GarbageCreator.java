/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.ictglobal.garbage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

/**
 *
 * @author tenhandsome
 */
public class GarbageCreator {

    public static final int NORMAL_CASE = 1;
    public static final int STRINGBUILDER_CASE = 2;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Charset encoding = Charset.defaultCharset();
        File file = new File("/Users/tenhandsome/NetBeansProjects/OtherProjects/src/hust/soict/ictglobal/garbage/data.txt");

        try {
            int option = STRINGBUILDER_CASE;
            //int option = NORMAL_CASE;

            new GarbageCreator().handleFile(file, encoding, option);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void handleFile(File file, Charset encoding, int option) throws IOException {
        try (InputStream in = new FileInputStream(file);
                Reader reader = new InputStreamReader(in, encoding);
                Reader buffer = new BufferedReader(reader)) {
            if (option == NORMAL_CASE) {
                this.handleCharactersInNormal(buffer);
            } else if (option == STRINGBUILDER_CASE) {
                this.handleCharactersInStringBuilder(buffer);

            }
        }
    }

    private void handleCharactersInNormal(Reader reader) throws IOException {
        int r;
        String content = "";
        while ((r = reader.read()) != -1) {
            char ch = (char) r;
            content += ch;
        }
        System.out.println(content);
    }

    private void handleCharactersInStringBuilder(Reader reader) throws IOException {
        int r;
        StringBuilder content = new StringBuilder("");
        while ((r = reader.read()) != -1) {
            char ch = (char) r;
            content.append(ch);
        }
        System.out.println(content);
    }
}
