
package hust.soict.ictglobal.aims.Aims;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.Order.Order;
import java.util.Scanner;

public class Aims {

    public static final int EXIT = 0;

    public static void main(String[] args) {

        int choice = -1;
        boolean isGetConsoleData = false;
        Order order = null;
        while (choice != EXIT) {
            showMenu();
            choice = Integer.parseInt(new Scanner(System.in).nextLine());

            switch (choice) {
                case 1:
                    order = new Order();
                    System.out.println("            Create OK");

                    break;
                case 2:
                    if (order != null) {

                        System.out.println("=====> 1. Book!");
                        System.out.println("=====> 2. Disc!");

                        System.out.println("=====> Other to quit");
                        int itemCreate = Integer.parseInt(new Scanner(System.in).nextLine());
                        switch (itemCreate) {
                            case 1:
                                isGetConsoleData = true;
                                Media book = new Book(isGetConsoleData);
                                order.addMedia(book);
                                System.out.println("Add done");
                                break;
                            case 2:
                                isGetConsoleData = true;
                                Media disc = new DigitalVideoDisc(isGetConsoleData);
                                order.addMedia(disc);
                                System.out.println("Add done");
                                break;
                            default:
                                System.out.println("Exit");
                                break;

                        }
                    } else {
                        System.out.println("             Please create order first!!!!!!");

                    }
                    break;
                case 3:
                    if (order != null) {
                        System.out.println("Enter ID :");
                        int isDelete = 0;
                        int id = Integer.parseInt(new Scanner(System.in).nextLine());
                        for (Media media : order.getItemsOrdered()) {
                            if (media.getId() == id) {
                                order.getItemsOrdered().remove(media);
                                isDelete++;
                            }
                        }
                        if (isDelete > 0) {
                            System.out.println("Delete successfully!");
                        }

                    } else {
                        System.out.println("             Please create order first!!!!!!");
                    }

                    break;
                case 4:
                    if (order != null) {
                        for (Media media : order.getItemsOrdered()) {
                            media.showData();
                        }
                    } else {
                        System.out.println("             Please create order first!!!!!!");

                    }
                    break;
                case 0:
                    System.out.println("Exit done!");
                    break;
                default:
                    System.out.println("Invalid option!Try again");
                    break;
            }

        }

    }

    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }
}
