
package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import hust.soict.ictglobal.aims.media.Media;
import java.util.Scanner;

public class Disc extends Media{
    private int length;
    private String director;

    public Disc(){
        
    }
    
     public Disc(boolean getConsoleData) {
        super(getConsoleData);
        if (getConsoleData) {
            System.out.print("Enter director : ");
            this.director = new Scanner(System.in).nextLine();
            System.out.print("Enter length : ");
            this.length = Integer.parseInt(new Scanner(System.in).nextLine());
        }
    }
    
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }
    
    
    
    @Override
    public void showData() {
    }
}
