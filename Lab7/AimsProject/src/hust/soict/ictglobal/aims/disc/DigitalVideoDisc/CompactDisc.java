package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import hust.soict.ictglobal.aims.definition.Playable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompactDisc extends Disc implements Playable {

    public String artist;
    public int length;
    public List<Track> tracks;

    public CompactDisc() {
        this.tracks = new ArrayList<>();
    }

    public CompactDisc(boolean isGetConsoleData) {
        super(isGetConsoleData);
        if (isGetConsoleData) {
            System.out.print("Enter artist : ");
            this.artist = new Scanner(System.in).nextLine();
            System.out.print("Enter length : ");
            this.length = Integer.parseInt(new Scanner(System.in).nextLine());

            System.out.print("Enter number of tracks: ");
            int numTrack = Integer.parseInt(new Scanner(System.in).nextLine());
            for (int i = 0; i < numTrack; i++) {
                this.tracks.add(new Track(Boolean.TRUE));
            }
        }
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public int getLength() {
        for (Track track : tracks) {
            this.length += track.getLenght();
        }
        return this.getLength();
    }

    public void setLength(int length) {
        this.length = length;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    public int addTrack(Track track) {
        for (Track existTrack : tracks) {
            if (existTrack.getTitle() == track.getTitle() && existTrack.getLenght() == track.getLenght()) {
                return -1;
            }
        }
        this.tracks.add(track);
        return 1;
    }

    public int removeTrack(Track track) {
        int counter = 0;
        for (Track existTrack : tracks) {
            if (existTrack.getTitle() == track.getTitle() && existTrack.getLenght() == track.getLenght()) {
                this.tracks.remove(existTrack);
                counter++;
            }
        }
        return counter;
    }

    @Override
    public void play() {
        for (Track track : tracks) {
            track.play();
        }
    }

}
