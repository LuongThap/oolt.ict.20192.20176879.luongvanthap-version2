package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Book extends Media {
//        private String title;
//        private String category;
//        private float cost;

    private List<String> authors = new ArrayList<String>();

    public Book() {
        super();
    }

    public Book(boolean getConsoleData) {
        super(getConsoleData);
        if (getConsoleData) {
            System.out.print("Enter number of authors: ");
            int numAuthor = Integer.parseInt(new Scanner(System.in).nextLine());
            for (int i = 0; i < numAuthor; i++) {
                System.out.print("Author's name " + (i + 1) + " is : ");
                String author = new Scanner(System.in).nextLine();
                this.authors.add(author);
            }
        }
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public int addAuthor(String authorName) { // 1: add successfully, -1 :get errors
        for (String author : this.authors) {
            if (authorName == author) {
                return -1;
            }
        }
        if (this.authors.add(authorName)) {
            return 1;
        }
        return -1;
    }

    public void removeAuthor(String authorName) {
        for (String author : this.authors) {
            if (author == authorName) {
                this.authors.remove(author);
            }
        }
    }

    @Override
    public void showData() {
        System.out.println("----------------------------------");
        System.out.println("ID is : " + this.getId());
        System.out.println("Title is : " + this.getTitle());
        System.out.println("Category is : " + this.getCategory());
        System.out.println("Cost is : " + this.getCost());
        System.out.println("Authors are : ");
        for (String author : authors) {
            System.out.print(author + "     ");
        }
        System.out.println();

    }

}
