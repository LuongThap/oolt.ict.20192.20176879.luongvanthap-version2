
package hust.soict.ictglobal.aims.definition;

public interface Playable {
    public void play();
}
