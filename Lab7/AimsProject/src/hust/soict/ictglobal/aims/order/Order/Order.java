package hust.soict.ictglobal.aims.order.Order;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Order {

    private List<Media> itemsOrdered = new ArrayList<>();

    public Order() {
    }

    public List<Media> getItemsOrdered() {
        return itemsOrdered;
    }

    public void setItemsOrdered(List<Media> itemsOrdered) {
        this.itemsOrdered = itemsOrdered;
    }

    public void addMedia(Media media) {
        
        // auto increase id and set to media
        int generateId = 0;
        for (Media itemMedia : itemsOrdered) {
            if (itemMedia.getId() > generateId) {
                generateId = itemMedia.getId();
            }
        }
        media.setId(generateId);
        this.itemsOrdered.add(media);
    }

    public void removeMedia(Media media) {
        this.itemsOrdered.remove(media);
    }

    public float getTotalCost() {
        float total = 0;
        for (Media media : itemsOrdered) {
            total += media.getCost();
        }
        return total;
    }

}
