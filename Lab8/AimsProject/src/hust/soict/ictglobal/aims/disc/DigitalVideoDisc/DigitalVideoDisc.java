/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import hust.soict.ictglobal.aims.definition.Playable;
import hust.soict.ictglobal.aims.media.Media;
import java.util.Scanner;

public class DigitalVideoDisc extends Disc implements Playable, Comparable {

  
    public DigitalVideoDisc() {
        super();
    }

    public DigitalVideoDisc(boolean getConsoleData) {
        super(getConsoleData);
       
    }

    public DigitalVideoDisc(String title) {
        super();
        this.setTitle(title);
    }

    public DigitalVideoDisc(String category, String title) {
        super();
        this.setCategory(category);
        this.setTitle(title);
    }

    public DigitalVideoDisc(String director, String category, String title) {
        super();
        this.setDirector(director);
        this.setCategory(category);
        this.setTitle(title);
    }

    public DigitalVideoDisc(String director, String category, String title, int length, float cost) {
        super();
        this.setDirector(director);
        this.setLength(length);
        this.setCategory(category);
        this.setTitle(title);
        this.setCost(cost);
    }

    public boolean search(String title) {
        if (this.getTitle().contains(title)) {
            return true;
        }
        return false;
    }

    @Override
    public void showData() {
        System.out.println("----------------------------------");
        System.out.println("ID is : " + this.getId());
        System.out.println("Title is : " + this.getTitle());
        System.out.println("Category is : " + this.getCategory());
        System.out.println("Cost is : " + this.getCost());
        System.out.println("Director is : " + this.getDirector());
        System.out.println("Length is : " + this.getLength());
        System.out.println();

    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    @Override
    public int compareTo(Object o) {
        return -1;
        
    }
}
