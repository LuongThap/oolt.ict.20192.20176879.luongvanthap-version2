/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practical.excercise;

import customize.exceptions.IllegalBirthDayException;
import customize.exceptions.IllegalGPAException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author tenhandsome
 */
public class Student {

    private int studentID;
    private String studentName;
    private String birthday;
    private float gpa;

    public Student() {
    }

    public Student(int studentID, String studentName, String birthday, float gpa) {
        this.studentID = studentID;
        this.studentName = studentName;
        this.birthday = birthday;
        this.gpa = gpa;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public float getGpa() {
        return gpa;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }

    private boolean isValidBirthday() {

        return true;
    }

    private boolean isValidGpa() {

        return true;
    }

    public void getConsoleData() throws IllegalBirthDayException, IllegalGPAException {
        
        System.out.println("Input StudentID : ");
        this.studentID = Integer.parseInt(new Scanner(System.in).nextLine());

        System.out.println("Input Student name : ");
        this.studentName = new Scanner(System.in).nextLine();

        System.out.println("Input birthday : ");
        this.birthday = new Scanner(System.in).nextLine();

        Pattern p = Pattern.compile("[0-9]{2})\\\\([0-9]{2})\\\\([0-9]{4}");
        Matcher m = p.matcher(this.birthday);
        if (!m.matches()) {
            throw (new IllegalBirthDayException());
        }
        System.out.println("Input gpa : ");
        this.gpa = Float.parseFloat(new Scanner(System.in).nextLine());
        if (this.gpa > 4 || this.gpa < 0) {
            throw (new IllegalGPAException());
        }

    }
}
