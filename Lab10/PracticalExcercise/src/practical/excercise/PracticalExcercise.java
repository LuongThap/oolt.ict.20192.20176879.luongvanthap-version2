/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practical.excercise;

import customize.exceptions.IllegalBirthDayException;
import customize.exceptions.IllegalGPAException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tenhandsome
 */
public class PracticalExcercise {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Student st = new Student();
        try {
            st.getConsoleData();
        } catch (IllegalBirthDayException ex) {
            ex.printStackTrace();
        } catch (IllegalGPAException ex) {
            ex.printStackTrace();
        } catch (NumberFormatException ex){
            ex.printStackTrace();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
}
