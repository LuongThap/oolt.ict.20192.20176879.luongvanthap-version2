/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customize.exceptions;

/**
 *
 * @author tenhandsome
 */
public class IllegalGPAException extends  Exception{

    @Override
    public void printStackTrace() {
        System.out.println("GPA value is not valid");
    }
    
    
}
