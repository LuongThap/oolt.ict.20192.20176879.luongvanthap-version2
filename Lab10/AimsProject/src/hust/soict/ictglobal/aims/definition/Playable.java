
package hust.soict.ictglobal.aims.definition;

import hust.soict.ictglobal.aims.exceptions.PlayerException;

public interface Playable {
    public void play() throws PlayerException;
}
