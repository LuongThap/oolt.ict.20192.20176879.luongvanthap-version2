/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.ictglobal.aims.media;

import java.util.Scanner;

/**
 *
 * @author tenhandsome
 */
public abstract class Media implements Comparable<Media>{

    private int id;
    private String title;
    private String category;
    private float cost;

    public Media() {

    }

    public Media(boolean getConsoleData) {
        if (getConsoleData) {
            System.out.print("Enter title : ");
            this.title = new Scanner(System.in).nextLine();

            System.out.print("Enter category : ");
            this.category = new Scanner(System.in).nextLine();

            System.out.print("Enter cost : ");
            this.cost = Float.parseFloat(new Scanner(System.in).nextLine());

        }
    }

    
    public Media(String title, String category, float cost) {
        this.title = title;
        this.category = category;
        this.cost = cost;
    }

    
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }
    
    
    public abstract void showData();

    @Override
    public boolean equals(Object media) {
        if (!(media instanceof  Media)) {
            throw (new ClassCastException());
        }
        if (media == null) {
            throw  (new NullPointerException());
        }
       Media m = (Media) media;
       return m.getTitle() == this.getTitle() && m.getCost() == this.getCost();
    }
    
    
    
}
