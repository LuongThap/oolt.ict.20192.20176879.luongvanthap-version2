package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Book extends Media {

    private String content;
    private List<String> contentToken = new ArrayList<>();
    private Map<String, Integer> wordFrequency = new HashMap<>();

    public List<String> getContentToken() {
        return contentToken;
    }

    public void setContentToken(List<String> contentToken) {
        this.contentToken = contentToken;
    }

    public Map<String, Integer> getWordFrequency() {
        return wordFrequency;
    }

    public void setWordFrequency(Map<String, Integer> wordFrequency) {
        this.wordFrequency = wordFrequency;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private List<String> authors = new ArrayList<String>();

    public Book() {
        super();
    }

    public Book(boolean getConsoleData) {
        super(getConsoleData);
        if (getConsoleData) {
            System.out.print("Enter number of authors: ");
            int numAuthor = Integer.parseInt(new Scanner(System.in).nextLine());
            for (int i = 0; i < numAuthor; i++) {
                System.out.print("Author's name " + (i + 1) + " is : ");
                String author = new Scanner(System.in).nextLine();
                this.authors.add(author);
            }
        }
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public int addAuthor(String authorName) { // 1: add successfully, -1 :get errors
        for (String author : this.authors) {
            if (authorName == author) {
                return -1;
            }
        }
        if (this.authors.add(authorName)) {
            return 1;
        }
        return -1;
    }

    public void removeAuthor(String authorName) {
        for (String author : this.authors) {
            if (author == authorName) {
                this.authors.remove(author);
            }
        }
    }

    @Override
    public void showData() {
        System.out.println("----------------------------------");
        System.out.println("ID is : " + this.getId());
        System.out.println("Title is : " + this.getTitle());
        System.out.println("Category is : " + this.getCategory());
        System.out.println("Cost is : " + this.getCost());
        System.out.println("Authors are : ");
        for (String author : authors) {
            System.out.print(author + "     ");
        }
        System.out.println();

    }

    public void processContent() {
        String[] dsWords = this.getContent() == null ? new String[0] : this.getContent().split(" ");
        if (dsWords.length > 0) {
            for (String word : dsWords) {
                this.contentToken.add(word);
                int couter = 0;
                for (String dupWord : dsWords) {
                    if (word.equals(dupWord)) {
                        couter++;
                    }
                }
                this.wordFrequency.put(word, couter);
            }
        }
    }

    @Override
    public String toString() {
        String au = "";
        for (String author : authors) {
            au += author + " - ";
        }
        
        String token = "";
        for (Map.Entry<String, Integer> element : wordFrequency.entrySet()) {
                token+= element.getKey() + " --- " + element.getValue() + "\n";
        }

        return new String(""
                + "ID :" + this.getId() + "\n"
                + "Title :" + this.getTitle()+ "\n"
                + "Cateogry :" + this.getCategory()+ "\n"
                + "Cost :" + this.getCost()+ "\n"
                + "Author :" + au+ "\n"
                + "Content length :" + this.getContent().length()+ "\n"
                + "Token list : \n" + token+ "\n"
        );
    }

}
