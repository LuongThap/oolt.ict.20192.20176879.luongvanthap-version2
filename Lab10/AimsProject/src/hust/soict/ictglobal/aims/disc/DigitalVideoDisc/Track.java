package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import hust.soict.ictglobal.aims.definition.Playable;
import hust.soict.ictglobal.aims.exceptions.PlayerException;
import java.util.Scanner;

public class Track implements Playable, Comparable {

    public String title;
    public int lenght;

    public Track() {
    }

    public Track(boolean isGetConsoleData) {
        if (isGetConsoleData) {
            System.out.print("Enter title : ");
            this.title = new Scanner(System.in).nextLine();
            System.out.print("Enter lenght : ");
            this.lenght = Integer.parseInt(new Scanner(System.in).nextLine());
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    @Override
    public void play() throws PlayerException{
        if (this.getLenght() <= 0) {
            System.err.println("ERROR : DVD length is 0");
            throw  (new PlayerException());
        }
        
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLenght());
    }

    @Override
    public int compareTo(Object o) {
        return -1;
    }
}
