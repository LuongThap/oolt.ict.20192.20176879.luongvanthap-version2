package hust.soict.ictglobal.aims.tracking;

public class MemoryDaemon implements Runnable {

    private long memoryUsed;

    public MemoryDaemon() {
        this.memoryUsed = 0;
    }

    public long getMemoryUsed() {
        return memoryUsed;
    }

    public void setMemoryUsed(long memoryUsed) {
        this.memoryUsed = memoryUsed;
    }

    @Override
    public void run() {
        Runtime rt = Runtime.getRuntime();
        long used = 0;
        while (true) {
            used = rt.totalMemory() - rt.freeMemory();
            if (used != memoryUsed) {
                System.out.println("Memory used : " + used);
                memoryUsed = used;
            }

        }
    }

}
