

import java.util.Arrays;
import java.util.Scanner;

public class Lab2_exer6 {

    public static void main(String[] args) {
        System.out.print("Enter number of array element : ");
        int num = Integer.parseInt(new Scanner(System.in).nextLine());
        int arr[] = new int[num];
        for (int i = 0; i < num; i++) {
            System.out.print("Enter number " + (i + 1) + " : ");
            arr[i] = Integer.parseInt(new Scanner(System.in).nextLine());
        }
        Arrays.sort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "  ");
        }
        System.out.println();
    }
}
