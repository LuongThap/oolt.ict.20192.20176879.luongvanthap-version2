
import java.time.YearMonth;
import java.util.Scanner;

public class Lab2_exer5 {

    public static void main(String[] args) {
        System.out.print("Enter year : ");
        int year = Integer.parseInt(new Scanner(System.in).nextLine());
        System.out.print("Enter month : ");
        int month = Integer.parseInt(new Scanner(System.in).nextLine());

        try {
            YearMonth y = YearMonth.of(year, month);
            int daysInMonth = y.lengthOfMonth();
            System.out.println("Day in month is : "+ daysInMonth);
        } catch (Exception e) {
            System.out.println("Invalid year or month");
        }

    }
}
