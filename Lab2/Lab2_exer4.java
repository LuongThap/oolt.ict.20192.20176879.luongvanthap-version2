
import java.util.Scanner;

public class Lab2_exer4 {

    public static void main(String[] args) {
		System.out.print("Please enter height : ");
        int height = Integer.parseInt(new Scanner(System.in).nextLine());
        int lastNumberOfStar = (height - 1) * 2 + 1;
        int j = 1;
        while (true) {
            int start = (lastNumberOfStar - j) / 2;
            for (int i = 0; i < lastNumberOfStar; i++) {
                if (i <= start || i > start + j) {
                    System.out.print(" ");
                } else {
                    System.out.print("*");
                }
            }
            System.out.print("\n");
            j += 2;
            if (j > lastNumberOfStar - 2) {
                break;
            }
        }
        System.out.print(" ");
        for (int i = 0; i < lastNumberOfStar; i++) {
            System.out.print("*");
        }
    }
}
